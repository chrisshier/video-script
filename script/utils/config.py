import yaml
import os

def read_config_from_yaml(file_name):
    # 获取当前脚本所在的目录
    script_dir = os.path.dirname(os.path.abspath(__file__))
    # 获取父级目录
    parent_dir = os.path.dirname(script_dir)
    # 构建文件路径
    file_path = os.path.join(parent_dir, '运行参数', file_name)

    print(f"尝试读取文件: {file_path}")  # 调试输出

    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            return yaml.safe_load(file)
    except FileNotFoundError:
        print(f"文件未找到: {file_path}")
    except PermissionError:
        print(f"没有权限读取文件: {file_path}")
    except yaml.YAMLError as e:
        print(f"YAML 解析错误: {e}")
    except Exception as e:
        print(f"未知错误: {e}")

if __name__ == '__main__':
    print(read_config_from_yaml("personification_config.yml"))
