import logging
import os
from datetime import datetime
from threading import currentThread


class Logger:
    def __init__(self, log_name='app', log_level=logging.DEBUG):
        # if log_name.startswith('127'):
        #     port = log_name.split(':')[1]
        #     log_name = "模拟器" + port
        # 获取或创建一个logger
        self.logger = logging.getLogger(log_name)
        self.logger.setLevel(log_level)

        # 确保日志目录存在
        log_dir = f'./logs/{log_name}'
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        # 创建一个handler，用于写入日志文件
        log_file_path = f'./logs/{log_name}/{datetime.now().strftime("%Y-%m-%d")}.log'
        fh = logging.FileHandler(log_file_path, encoding='utf-8')
        fh.setLevel(log_level)

        # 定义handler的输出格式
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)

        # 给logger添加handler
        self.logger.addHandler(fh)
        print(f"初始化日志实例对象: {log_name}, 线程：{currentThread().name}")

    def get_logger(self):
        return self.logger
