import subprocess

def get_devices():
    # ADB 命令，这里列出所有已连接的设备
    command = "adb devices"
    # 执行 ADB 命令
    try:
        # 使用 check_output 来执行 ADB 命令
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True, text=True)
        devices = output.splitlines()[1:]
        if len(devices) == 0 or (len(devices) == 1 and devices[0].strip() == ""):
            print("没有设备连接")
            return []
        return devices
    except subprocess.CalledProcessError as e:
        # 如果命令执行失败，返回错误信息
        print(f"命令执行失败: {e}")
        return []

def total_devices(devices):
    # 初始化计数器
    totaldevices = 0
    connected_devices = 0
    for line in devices:
        if line.strip():  # 如果这一行不是空行
            totaldevices += 1
            if 'device' in line:
                connected_devices += 1
    print(f"设备列表：", devices)
    print(f"设备总数: {totaldevices}, 已连接设备: {connected_devices}")