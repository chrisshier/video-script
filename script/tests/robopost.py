import subprocess
import time

import uiautomator2 as u2
import os
import qianfan
import asyncio
import random
from datetime import datetime, time, timedelta

device_list = []
package_name = "com.smile.gifmaker"
tasks = {}

def generatedText():
    # 通过环境变量初始化认证信息
    # 方式一：【推荐】使用安全认证AK/SK鉴权
    # 替换下列示例中参数，安全认证Access Key替换your_iam_ak，Secret Key替换your_iam_sk，如何获取请查看https://cloud.baidu.com/doc/Reference/s/9jwvz2egb
    os.environ["QIANFAN_ACCESS_KEY"] = ""
    os.environ["QIANFAN_SECRET_KEY"] = ""
    chat_comp = qianfan.ChatCompletion()
    # 指定特定模型
    resp = chat_comp.do(model="ERNIE-3.5-8K", messages=[{
        "role": "user",
        "content": "随机生成文案，人生哲理,人生感悟名人名言, 50字左右"
    }])

    try:
        # 提取 result 字段
        result = resp["body"].get("result")
        if result:
            print("获取到的文案: ", result)
            return result
    except Exception as e:
        print("生成文本失败:", e)
        return ""

def rebopost(device_serial):
    print("自动发布视频")
    # 初始化设备连接
    device = u2.connect(device_serial)
    # 启动App
    device.session(package_name, attach=True)
    # 点击发布按钮
    device(resourceId=f"{package_name}:id/default_btn_shoot_container").click(timeout=60)
    # 点击发布文字视频
    device(resourceId=f"{package_name}:id/mood_tab_tv", index=2).click(timeout=60)
    # 点击输入文字
    editor_view = device(resourceId=f"{package_name}.story:id/text_input_edit_view")
    editor_view_exists = editor_view.exists(timeout=5)
    if editor_view_exists:
        text = generatedText()
        editor_view.send_keys(text)
        time.sleep(5)
        device(resourceId=f"{package_name}.story:id/text_edit_complete_button", index=2).click(timeout=60)
        publish_button = device(resourceId=f"{package_name}.story:id/done_btn")
        while True:
            publish_button_exists = publish_button.exists(timeout=2)
            if publish_button_exists:
                time.sleep(5)
                print("点击发布按钮")
                publish_button.click()
                break
            print("未找到发布按钮，正在重试...")

    time.sleep(60)
    device.app_stop(package_name)

def init():
    global device_list
    # ADB 命令，这里列出所有已连接的设备
    command = "adb devices"
    # 执行 ADB 命令
    device_list = run_adb_command(command).splitlines()[1:]
    if len(device_list) == 0 or (len(device_list) == 1 and device_list[0].strip() == ""):
        print("没有设备连接")
        return

def run_adb_command(command):
    try:
        # 使用 check_output 来执行 ADB 命令
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True, text=True)
        return output
    except subprocess.CalledProcessError as e:
        # 如果命令执行失败，返回错误信息
        return e.output

async def main():
    global tasks
    init()
    print("开始执行")
    for device in device_list:
        if device.strip() == "":
            continue
        device_serial = device.strip().split("\t")[0]
        device_state = device.strip().split("\t")[1]
        print(device_serial, device_state)
        if device_state != "device":
            print(f"{device_serial} 设备未连接")
            continue

        tasks[device_serial] = asyncio.create_task(rebopost(device_serial))
    # 等待所有任务完成
    await asyncio.gather(*tasks)


# 生成随机时间
def generate_random_time(start_hour, end_hour):
    start_time = time(start_hour, 0, 0)
    end_time = time(end_hour, 0, 0)
    start_seconds = (start_time.hour * 3600) + (start_time.minute * 60) + start_time.second
    end_seconds = (end_time.hour * 3600) + (end_time.minute * 60) + end_time.second
    random_seconds = random.randint(start_seconds, end_seconds)
    return (datetime.min + timedelta(seconds=random_seconds)).time()


# 生成一天中的三个随机时间
def generate_daily_times():
    morning_time = generate_random_time(6, 12)
    afternoon_time = generate_random_time(12, 18)
    evening_time = generate_random_time(18, 23)
    return morning_time, afternoon_time, evening_time


# 检查当前时间是否在指定的时间范围内
def is_time_to_run(current_time, target_time):
    return current_time.hour == target_time.hour and current_time.minute == target_time.minute


# 主循环
async def run_scheduler():
    while True:
        # 生成当天的三个随机时间
        morning_time, afternoon_time, evening_time = generate_daily_times()

        print(f"生成的时间: 早上: {morning_time}, 下午: {afternoon_time}, 晚上: {evening_time}")

        while True:
            current_time = datetime.now().time()

            if is_time_to_run(current_time, morning_time) or \
                    is_time_to_run(current_time, afternoon_time) or \
                    is_time_to_run(current_time, evening_time):
                await main()
                break

            await asyncio.sleep(60)  # 每分钟检查一次

if __name__ == "__main__":
    asyncio.run(run_scheduler())