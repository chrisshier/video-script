import subprocess
import time
import json

# 初始化设备列表和状态字典
device_list = []
device_status = {}

# 文件路径
file_path = 'device_list.json'

def get_connected_devices():
    """获取当前连接的所有设备"""
    result = subprocess.run(['adb', 'devices'], stdout=subprocess.PIPE)
    devices = result.stdout.decode('utf-8').strip().split('\n')[1:]
    return [device.split('\t')[0] for device in devices]

def save_device_list():
    """将设备列表和状态保存到本地文件"""
    with open(file_path, 'w') as f:
        json.dump({'device_list': device_list, 'device_status': device_status}, f, indent=4)

def load_device_list():
    """从本地文件加载设备列表和状态"""
    try:
        with open(file_path, 'r') as f:
            data = json.load(f)
            global device_list, device_status
            device_list = data['device_list']
            device_status = data['device_status']
    except FileNotFoundError:
        pass

def handle_device_changes(current_devices):
    """处理设备变化"""
    global device_list, device_status

    # 检查新增设备
    new_devices = set(current_devices) - set(device_list)
    for device in new_devices:
        device_list.append(device)
        device_status[device] = 'online'
        print(f"新设备加入: {device} (第{device_list.index(device) + 1}台)")

    # 检查离线设备
    offline_devices = set(device_list) - set(current_devices)
    for device in offline_devices:
        if device_status.get(device) == 'online':
            index = device_list.index(device) + 1
            print(f"设备离线: {device} (第{index}台)")
            device_status[device] = 'offline'

    # 检查重新连接的设备
    reconnected_devices = set(current_devices) & set(device_list)
    for device in reconnected_devices:
        if device_status.get(device) == 'offline':
            index = device_list.index(device) + 1
            print(f"设备重新连接: {device} (第{index}台)")
            device_status[device] = 'online'

def print_device_status():
    """打印设备状态"""
    online_devices = [device for device, status in device_status.items() if status == 'online']
    offline_devices = [device for device, status in device_status.items() if status == 'offline']

    print(f"总设备数: {len(device_list)}")
    print(f"在线设备数: {len(online_devices)}")
    # for device in online_devices:
    #     index = device_list.index(device) + 1
    #     print(f"  在线设备: {device} (第{index}台)")

    print(f"离线设备数: {len(offline_devices)}")
    for device in offline_devices:
        index = device_list.index(device) + 1
        print(f"  离线设备: {device} (第{index}台)")

def main():
    load_device_list()  # 加载设备列表和状态
    while True:
        current_devices = get_connected_devices()
        handle_device_changes(current_devices)
        save_device_list()  # 保存设备列表和状态
        print_device_status()  # 打印设备状态
        time.sleep(2)  # 每2秒检查一次

if __name__ == "__main__":
    main()
