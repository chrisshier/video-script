import subprocess

def check_online_device():
    # ADB 命令，这里列出所有已连接的设备
    command = "adb devices"
    # 执行 ADB 命令
    device_list = run_adb_command(command).splitlines()[1:]
    if len(device_list) == 0 or (len(device_list) == 1 and device_list[0].strip() == ""):
        print("没有设备连接")
    # 统计设备
    return total_devices(device_list)

def run_adb_command(command):
    try:
        # 使用 check_output 来执行 ADB 命令
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True, text=True)
        return output
    except subprocess.CalledProcessError as e:
        # 如果命令执行失败，返回错误信息
        return e.output

def total_devices(device_list):
    # 初始化计数器
    total_devices = 0
    connected_devices = 0
    for line in device_list:
        if line.strip():  # 如果这一行不是空行
            total_devices += 1
            if 'device' in line:
                connected_devices += 1
    print(f"设备列表：", device_list)
    print(f"设备总数: {total_devices}, 已连接设备: {connected_devices}")
    return device_list, total_devices, connected_devices

if __name__ == '__main__':
    check_online_device()
    input("按任意键退出...")