import uuid

import uiautomator2 as u2
import time
from datetime import datetime, timedelta
import random

from script.utils.log import Logger

# 返回标志
back_flag = False
# 监控事件停止标志
stop_monitor_flag = False
# 刷视频的间隔时间
sleep_video_time = []
# 广告停留时间
sleep_advertisement_time = []


class AppSwitcher:
    def __init__(self, device_serial, app_types, search_id, app_switch_interval, current_comment_id,user_slide_count):
        # 初始化设备连接
        self.device = u2.connect(device_serial)
        # 获取系统名称和安卓版本
        self.system = self.device.shell("getprop ro.product.brand").output
        self.android_version = self.device.shell("getprop ro.build.version.release").output
        self.device_serial = device_serial
        print(f"设备序列号：{device_serial}, 系统名称：{self.system}, 系统版本：{self.android_version}")
        # 入口id
        self.ks_id = search_id
        # 设置
        self.setting()
        # 当前应用包名
        self.package_name = ""
        # 应用类型标识（0为快手，1为极速版）
        self.app_type = 0
        # 用户滑动总次数
        self.user_slide_count = user_slide_count
        # 总滑动次数
        self.swipe_count = 0
        # 快手滑动次数
        self.swipe_count_kuai_shou = 0
        # 极速版滑动次数
        self.swipe_count_ji_su = 0
        # 用户名初始化
        self.user_name = "@"
        # 用户快手号初始化
        self.user_ks_id = ''
        # 不匹配的次数
        self.not_match_count = 0
        # 应用类型列表
        self.app_types = app_types
        # 用户名是否存在, 0为正常，超过3次为不匹配
        self.user_name_exist = 0
        # 是否回退
        self.is_back = False
        # 日志
        self.logger = Logger(log_name=device_serial).get_logger()
        # 应用开始时间
        self.start_time = datetime.now()
        # 切换应用的时间间隔
        self.app_switch_interval = app_switch_interval
        # 当前刷的账号在评论区的序号
        self.current_comment_id = current_comment_id
        # 应用配置字典
        self.apps = {
            "": ("com.smile.gifmaker", "初次启动", 0),
            "com.smile.gifmaker": ("com.kuaishou.nebula", "切换到极速版", 1),
            "com.kuaishou.nebula": ("com.smile.gifmaker", "切换到快手", 0),
        }
        self.ctx = None

    def initConfig(self):
        global sleep_video_time, sleep_advertisement_time
        try:
            from script.utils.config import read_config_from_yaml
            yaml_config = read_config_from_yaml("switcher_config.yml")
            sleep_video_time = yaml_config['sleep_video_time']
            sleep_advertisement_time = yaml_config['sleep_advertisement_time']
            print(f"读取配置成功，视频播放时间范围：{sleep_video_time}, 广告停留时间范围：{sleep_advertisement_time}")
            try:
                if sleep_video_time['min'] > sleep_video_time['max'] or sleep_advertisement_time['min'] > sleep_advertisement_time['max']:
                    raise ValueError("随机数最小值min应小于等于最大值max")
            except ValueError as e:
                print(f"时间范围无效: {e}")
        except Exception as e:
            print(f"读取配置或等待时间出错，错误信息：{e}")
            time.sleep(5)  # 出错时默认等待5秒

    def setting(self):
        # http默认请求超时时间, 5分钟
        self.device.HTTP_TIMEOUT = 300
        # 当设备掉线时，等待设备在线时长
        self.device.WAIT_FOR_DEVICE_TIMEOUT = 300

    def update_ks_id(self, ks_id, current_comment_id):
        self.ks_id = ks_id
        self.current_comment_id = current_comment_id

    def start_app(self, package_name):
        # 启动或附加到指定的应用程序会话
        session = self.device.session(package_name, attach=True)

    # 指定用户的总滑动次数
    def increment_user_slide_count(self):
        try:
            if self.user_name != '@' and self.user_name != '' :
                if self.user_ks_id not in self.user_slide_count:
                    self.user_slide_count[self.user_ks_id] = {}
                if self.user_name not in self.user_slide_count[self.user_ks_id]:
                    self.user_slide_count[self.user_ks_id][self.user_name] = 0

            # 增加该评论区用户滑动的次数
            self.user_slide_count[self.user_ks_id][self.user_name] += 1

            for comment_id, users in self.user_slide_count.items():
                for user, count in users.items():
                    self.logger.info(f"评论区 {comment_id} 中，用户 {user} 已滑动 {count} 次")
        except Exception as e:
            self.logger.error(f"用户滑动次数统计出错：{e}")

    def swipe(self):
        # 增加总滑动计数
        self.swipe_count += 1
        # 如果是快手，则增加快手滑动计数
        if self.app_type == 0:
            self.swipe_count_kuai_shou += 1
        # 如果是极速版，则增加极速版滑动计数
        else:
            self.swipe_count_ji_su += 1
        # 获取屏幕尺寸
        x, y = self.device.window_size()
        # 计算屏幕中心的x坐标
        x_center = x / 2
        # 计算滑动起点y坐标
        y_start = y * 0.8
        # 计算滑动终点y坐标
        y_end = y * 0.1
        # 执行滑动操作
        self.device.swipe(x_center, y_start, x_center, y_end, 0.1)
        # 输出滑动统计信息
        self.print_swipe_counts()

    def swipe_down(self):
        # 获取屏幕尺寸
        x, y = self.device.window_size()
        # 计算屏幕中心的x坐标
        x_center = x / 2
        # 计算滑动起点y坐标
        y_start = y * 0.9
        # 计算滑动终点y坐标
        y_end = y * 0.2
        # 执行滑动操作
        self.device.swipe(x_center, y_end, x_center, y_start, 0.1)

    def swipe_up(self):
        # 获取屏幕尺寸
        x, y = self.device.window_size()
        # 计算屏幕中心的x坐标
        x_center = x / 2
        # 计算滑动起点y坐标
        y_start = y * 0.1
        # 计算滑动终点y坐标
        y_end = y * 0.9
        # 执行滑动操作
        self.device.swipe(x_center, y_end, x_center, y_start, 0.1)

    def print_swipe_counts(self):
        # 输出当前滑动视频总次数
        self.logger.info(f"当前滑动视频总次数：%s 次", self.swipe_count)
        # 输出当前滑动视频次数-快手
        self.logger.info(f"当前滑动视频次数-快手：%s 次", self.swipe_count_kuai_shou)
        # 输出当前滑动视频次数-极速版
        self.logger.info(f"当前滑动视频次数-极速版：%s 次", self.swipe_count_ji_su)

    def search_id(self, id):
        # 根据应用类型选择搜索方式
        if self.app_type == 0:
            self.select_and_search(self.package_name, id)
        else:
            self.search_directly(self.package_name, id)

    def select_and_search(self, app_package, id):
        # 点击默认搜索视图
        search_view1 = self.device(resourceId=f"{app_package}:id/search_btn")
        search_view2 = self.device(resourceId=f"{app_package}:id/nasa_featured_default_search_view")
        search_view_exists1 = search_view1.exists(timeout=5)
        search_view_exists2 = search_view2.exists(timeout=5)
        if search_view_exists1:
            print("点击首页搜索视图")
            search_view1.click()
        if search_view_exists2:
            print("点击精选搜索视图")
            search_view2.click()
        # 输入搜索ID
        editor_view = self.device(resourceId=f"{app_package}:id/editor")
        editor_view_exists = editor_view.exists(timeout=5)
        if editor_view_exists:
            editor_view.send_keys(id)
            search_button = self.device(text="搜索")
            search_button_exists = search_button.exists(timeout=5)
            if search_button_exists:
                search_button.click()
        # 点击第一个搜索结果的名字
        search_result_view = self.device(index=0, resourceId=f"{app_package}:id/item_root")
        search_result_view_exists = search_result_view.exists(timeout=5)
        if search_result_view_exists:
            search_result_view.click()
        user_view = self.device(text="用户")
        user_view_exists = user_view.exists(timeout=5)
        if user_view_exists:
            user_view.click()
        name_view = self.device(resourceId=f"{app_package}:id/name")
        name_view_exists = name_view.exists(timeout=5)
        if name_view_exists:
            name_view.click()
        # 点击视频封面
        player_view = self.device(index=0, resourceId=f"{app_package}:id/player_cover_container")
        player_view_exists = player_view.exists(timeout=5)
        if player_view_exists:
            player_view.click()
        # 点击评论图标
        comment_view = self.device(resourceId=f"{app_package}:id/comment_icon")
        comment_view_exists = comment_view.exists(timeout=5)
        if comment_view_exists:
            comment_view.click()
        # 点击用户名
        name_view = self.device(index=self.current_comment_id, resourceId=f"{app_package}:id/comment_frame").child(index=0, resourceId=f'{app_package}:id/name')
        name_view_exists = name_view.exists(timeout=5)
        if name_view_exists:
            # 获取用户名
            self.user_name += name_view.get_text()
            name_view.click()
        # 输出用户名
        #self.logger.info(self.user_name)
        # 获取用户快手号
        user_ks_id = self.device(resourceId=f"{app_package}:id/profile_user_kwai_id")
        user_ks_id_exists = user_ks_id.exists(timeout=5)
        if user_ks_id_exists:
            full_name = user_ks_id.get_text()
            self.user_ks_id = full_name.replace("快手号：", "").strip()
        # 输出用户快手号
        self.logger.info(f"评论区序号： {self.current_comment_id} ，用户名： {self.user_name} ，快手号： {self.user_ks_id}")
        # 再次点击视频封面
        player_cover_container = self.device(index=0, resourceId=f"{app_package}:id/player_cover_container")
        player_cover_container_exists = player_cover_container.exists(timeout=5)
        if player_cover_container_exists:
            player_cover_container.click()

    def search_directly(self, app_package, id):
        # 点击顶部搜索栏
        top_search_view = self.device(index=0, resourceId=f"{app_package}:id/thanos_home_top_search")
        top_search_view_exists = top_search_view.exists(timeout=5)
        if top_search_view_exists:
            top_search_view.click()
        # 输入搜索ID
        editor_view = self.device(resourceId=f"{app_package}:id/editor")
        editor_view_exists = editor_view.exists(timeout=5)
        if editor_view_exists:
            editor_view.send_keys(id)
            search_button = self.device(text="搜索")
            search_button_exists = search_button.exists(timeout=5)
            if search_button_exists:
                search_button.click()
        # 点击第一个搜索结果的名字
        search_result_view = self.device(index=0, resourceId=f"{app_package}:id/item_root")
        search_result_view_exists = search_result_view.exists(timeout=5)
        if search_result_view_exists:
            search_result_view.click()
        # 点击“用户”
        user_view = self.device(text="用户")
        user_view_exists = user_view.exists(timeout=5)
        if user_view_exists:
            user_view.click()
        # 点击用户名
        name_view = self.device(resourceId=f"{app_package}:id/name")
        name_view_exists = name_view.exists(timeout=5)
        if name_view_exists:
            name_view.click()
        # 点击视频封面
        player_view = self.device(index=0, resourceId=f"{app_package}:id/player_cover_container")
        player_view_exists = player_view.exists(timeout=5)
        if player_view_exists:
            player_view.click()
        # 点击评论图标
        comment_view = self.device(resourceId=f"{app_package}:id/comment_icon")
        comment_view_exists = comment_view.exists(timeout=5)
        if comment_view_exists:
            comment_view.click()
        # 点击用户名
        name_view = self.device(index=self.current_comment_id, resourceId=f"{app_package}:id/comment_frame").child(index=0, resourceId=f'{app_package}:id/name')
        name_view_exists = name_view.exists(timeout=5)
        if name_view_exists:
            # 获取用户名
            self.user_name += name_view.get_text()
            name_view.click()
        #self.logger.info(self.user_name)
        # 获取用户快手号
        user_ks_id = self.device(resourceId=f"{app_package}:id/profile_user_kwai_id")
        user_ks_id_exists = user_ks_id.exists(timeout=5)
        if user_ks_id_exists:
            full_name = user_ks_id.get_text()
            self.user_ks_id = full_name.replace("快手号：", "").strip()
        # 输出用户快手号
        self.logger.info(f"评论区序号： {self.current_comment_id} ，用户名： {self.user_name} ，快手号： {self.user_ks_id}")
        # 再次点击视频封面
        player_cover_container = self.device(index=0, resourceId=f"{app_package}:id/player_cover_container")
        player_cover_container_exists = player_cover_container.exists(timeout=5)
        if player_cover_container_exists:
            player_cover_container.click()

    def job(self, retry=0):
        global stop_monitor_flag
        stop_monitor_flag = True
        self.user_name = "@"
        # 获取下一个应用的信息
        new_package_name, message, new_app_type = self.apps.get(self.package_name, (None, None, None))
        # 重试当前应用
        if retry == 1:
            new_package_name, message, new_app_type = self.apps.get(new_package_name, (None, None, None))

        if new_package_name is not None:
            if self.package_name != '':
                self.device.app_stop(self.package_name)
            # 输出切换应用的消息
            self.logger.info(message)
            # 如果当前应用不是空，则停止当前应用
            self.device.app_stop(new_package_name)
            # 等待5秒
            time.sleep(5)
            # 设置新的应用包名
            self.package_name = new_package_name
            # 设置新的应用类型
            self.app_type = new_app_type
            # 启动新应用
            self.start_app(self.package_name)
            try:
                self.logger.info(f"当前page_name: %s", self.package_name)
                # 执行搜索操作
                self.search_id(self.ks_id)
                stop_monitor_flag = False
            except Exception as e:
                # 异常处理：如果出现异常，则重启应用
                self.logger.error("检测到界面异常，重启应用, 错误信息：%s", e)
                # self.device.screenshot('./img/' + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.png')
                self.job(1)

    def start_monitoring(self):
        # 创建设备监控上下文
        self.ctx = self.device.watch_context()
        self.handle_welcome_screen()
        self.handle_popup()

    def handle_welcome_screen(self):
        try:
            # 监听“跳过”按钮
            self.ctx.when("跳过").call(lambda d: d(text="跳过").click_gone(maxretry=1, interval=0.1))
            # 监听“同意，并领新手红包”按钮
            self.ctx.when("同意，并领新手红包").call(
                lambda d: d(text="同意，并领新手红包").click_gone(maxretry=1, interval=0.1))
            # 监听“同意并继续”按钮
            self.ctx.when("同意并继续").call(lambda d: d(text="同意并继续").click_gone(maxretry=1, interval=0.1))
            # 监听朋友推荐
            self.ctx.when("朋友推荐").call(lambda d: d(resourceId="com.smile.gifmaker:id/close_btn").click_gone(maxretry=1, interval=0.1))
            self.ctx.when("聊天频道").call(self.swipe_up)

            self.ctx.when("更多直播").call(self.swipe_up)
            # # 开启弹窗监控，并等待界面稳定（两个弹窗检查周期内没有弹窗代表稳定）
            # ctx.wait_stable()
        except Exception as e:
            # 如果没有找到“跳过”按钮，则输出提示并继续执行
            self.logger.error("未找到'跳过'按钮，继续执行后续操作... %s", e)

    def handle_popup(self):
        # 点击重试
        self.ctx.when("点击重试").call(lambda d: d(text="点击重试").click_gone(maxretry=1, interval=0.1))
        # 监听“暂不关注”按钮
        self.ctx.when("暂不关注").call(lambda d: d(text="暂不关注").click_gone(maxretry=1, interval=0.1))
        # 监听“弹幕开关”和“自动上滑”按钮，模拟按下返回键
        self.ctx.when("弹幕开关").when("自动上滑").call(lambda d: d.press("back"))
        self.ctx.when("朋友推荐").call(lambda d: d.press("back"))
        # 监听“无更多作品”消息，调用循环方法
        self.ctx.when("无更多作品").call(self.back)
        self.ctx.when("分享给朋友").when("分享到微信").call(lambda d: d.press("back"))
        # 监听“关闭”按钮
        self.ctx.when("关闭").call(lambda d: d(text="关闭").click_gone(maxretry=1, interval=0.1))
        # 监听“关闭应用”按钮
        self.ctx.when("关闭应用").call(lambda d: d(text="关闭应用").click_gone(maxretry=1, interval=0.1))

    def check_advertisement(self):
        global sleep_advertisement_time
        is_exists_user_name = False
        user_name = ''
        # 尝试获取当前视频的用户名
        try:
            user_name_text_view = self.device(className="android.widget.TextView",
                                              resourceId=f"{self.package_name}:id/user_name_text_view")
            user_name_text_view_exists = user_name_text_view.exists(timeout=5)
            if user_name_text_view_exists:
                is_exists_user_name = True
                user_name = user_name_text_view.get_text(2)
            else:
                is_exists_user_name = False
        except Exception as e:
            is_exists_user_name = False
            self.logger.error("获取不到用户名：%s", e)

        try:
            if is_exists_user_name:
                self.user_name_exist = 0
            else:
                self.user_name_exist += 1
                print(f"设备：{self.device_serial}，用户名不存在次数：{self.user_name_exist}")
            if self.user_name_exist >= 5:
                raise Exception("检测到用户名不存在，当前可能不在目标用户的视频列表中，重启应用")
            # 检查用户名是否发生变化，判断是否为广告
            if user_name != '' and user_name != self.user_name:
                self.logger.info("识别为广告")
                self.logger.info("用户名：%s", user_name)
                self.logger.info("目标用户名：%s", self.user_name)
                if sleep_advertisement_time['complete']:
                    try:
                        start_time = time.time()  # 记录开始时间
                        end_max_time = start_time + 120  # 设置最晚结束时间
                        while time.time() < end_max_time:
                            # 执行你的操作
                            if self.device(text="点击重播").exists(timeout=5):
                                break
                        end_time = time.time()  # 记录结束时间
                        elapsed_time = end_time - start_time  # 计算耗时

                        print(f"操作耗时: {elapsed_time:.2f} 秒")
                    except Exception as e:
                        self.logger.error(f"广告检测超时：{e}")
                else:
                    # 随机等待时间
                    time.sleep(random.uniform(sleep_advertisement_time['min'], sleep_advertisement_time['max']))

                self.not_match_count += 1
                if self.not_match_count >= 5:
                    raise Exception("检测到当前可能不在目标用户的视频列表中，重启应用")
            else:
                self.not_match_count = 0
        # 如果发生异常，则认为界面异常
        except Exception as e:
            print("界面异常，即将重启应用")
            self.not_match_count = 0
            self.logger.error("检测到界面异常，重启应用，错误信息：%s", e)
            # self.device.screenshot('./img/' + datetime.now().strftime("%Y-%m-%d %H-%M-%S") + '.png')
            # 重新启动应用
            self.job(1)

    def loop_brush(self):
        global sleep_video_time
        while back_flag:
            # print("等待回退")
            time.sleep(1)
        # 获取当前时间
        current_time = datetime.now()
        print(f"设备：{self.device_serial}, 循环次数：{self.swipe_count + 1}")
        # 检查是否已经运行了30分钟
        if (current_time - self.start_time) >= timedelta(minutes=self.app_switch_interval):
            self.logger.info("切换到下一个应用")
            print(f"设备：{self.device_serial}, 切换到下一个应用")
            self.start_time = datetime.now()
            # 输出滑动统计信息
            self.print_swipe_counts()
            # 切换应用
            self.job()
        # 执行一次滑动操作
        if not back_flag:
            self.swipe()
        # 视频随机播放时间
        time.sleep(random.uniform(sleep_video_time['min'], sleep_video_time['max']))

        # 判断是否为广告
        current_time1 = datetime.now()
        if not back_flag:
            self.check_advertisement()
        current_time2 = datetime.now()
        print(f"设备：{self.device_serial}, 广告检测时间：{(current_time2 - current_time1).total_seconds()}")

    def main(self):
        self.logger.info("当前刷的快手号为：%s", self.ks_id)
        # 获取配置信息
        self.initConfig()
        # 开启监控
        if self.ctx is None:
            self.start_monitoring()
        else:
            print("监控已开启")
        # 启动初始应用
        self.job()
        # 获取当前时间作为开始时间
        self.start_time = datetime.now()
        print(f"设备：{self.device_serial}, 开始时间：{self.start_time}")

    def back(self):
        global back_flag, stop_monitor_flag
        print(f"当前监控标志：{stop_monitor_flag}")
        back_flag = True
        self.device.press("back")
        time.sleep(2)
        user_name_view = self.device(text="关注")
        user_name_view_exists = user_name_view.exists(timeout=2)
        while not user_name_view_exists:
            if stop_monitor_flag:
                print("返回事件线程结束2")
                back_flag = False
                return
            print('下拉')
            self.swipe_down()
            user_name_view_exists = user_name_view.exists(timeout=2)
        player_view = self.device(resourceId=f'{self.package_name}:id/player_cover_container')
        player_view_exists = player_view.exists(timeout=10)
        if player_view_exists:
            player_view.click()
            back_flag = False

    def stop_app(self):
        self.logger.info("关闭应用")
        self.device.app_stop("com.smile.gifmaker")
        self.device.app_stop("com.kuaishou.nebula")
        print(f"设备：{self.device_serial}, 关闭应用")


if __name__ == '__main__':
    d = u2.connect("P202408093563")
    #d.screenshot(f'P202408093563:{uuid.uuid4()}.png')
    datetime1 = datetime.now()
    search_view = d(resourceId='com.kuaishou.nebula.search_feature:id/search_result_text')
    search_view_exists = search_view.exists(timeout=5)
    datetime2 = datetime.now()
    print(f"设备：{d.serial}, 搜索按钮是否存在：{search_view_exists}, 耗时：{(datetime2 - datetime1).total_seconds()}")
    if search_view_exists:
        print("存在")
        search_view.click()
