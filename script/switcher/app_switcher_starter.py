import subprocess
from concurrent.futures import ThreadPoolExecutor, as_completed
from threading import current_thread
from script.switcher.app_switcher import AppSwitcher
import threading
import time
from script.utils.config import read_config_from_yaml

from script.utils.log import Logger

# 共享变量
slide_count = 0
# 全局字典来记录每个评论id和用户名的刷次数
user_slide_count = {}
# 停止标志
stop_flag = False
# 总开关
main_switch = True
# 日志
log = None
# 当前刷的快手号
current_ks_id = ''
# 当前刷的是评论区第几个评论账号
current_comment_id = 0
# 设备列表
device_list = []
# 配置信息
config = {}
# 任务列表，每台设备对应一个任务
tasks = {}
# AppSwitcher 实例字典
switchers = {}
# 线程池
executor = ThreadPoolExecutor(max_workers=10)  # 初始最大线程数

# 定时任务函数
def check_slide_count(times):
    global main_switch, slide_count, stop_flag, current_ks_id, current_comment_id
    while main_switch:
        print(f"检查刷的次数，{slide_count}")
        if slide_count > times:
            stop_flag = True
            log.info("快手号：%s 评论区第%s个评论的账号，已刷够次数，%s 次", current_ks_id, current_comment_id, times)
            print(f"快手号：{current_ks_id} 评论区第{current_comment_id}个评论的账号，已刷够次数，{times} 次")
        time.sleep(5)  # 每秒检查一次

def run_adb_command(command):
    try:
        # 使用 check_output 来执行 ADB 命令
        output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True, text=True)
        return output
    except subprocess.CalledProcessError as e:
        # 如果命令执行失败，返回错误信息
        return e.output

def total_devices(device_list):
    # 初始化计数器
    total_devices = 0
    connected_devices = 0
    for line in device_list:
        if line.strip():  # 如果这一行不是空行
            total_devices += 1
            if 'device' in line:
                connected_devices += 1
    log.info(f"设备列表：%s", device_list)
    log.info(f"设备总数: %s, 已连接设备: %s", total_devices, connected_devices)
    print(f"设备列表：", device_list)
    print(f"设备总数: {total_devices}, 已连接设备: {connected_devices}")

def run_main(switcher, device_serial):
    global stop_flag, slide_count, tasks, user_slide_count
    try:
        switcher.main()
        # 主循环
        while not stop_flag:
            switcher.increment_user_slide_count()
            slide_count += 1
            switcher.loop_brush()
        for ks_id, users in user_slide_count.items():
             for user, count in users.items():
                 log.info(f"设备: [{device_serial}] ，快手号： {ks_id} ，用户：{user} ，共刷了 {count} 次")
    except Exception as e:
        log.error(f"设备 %s 运行程序异常: %s", device_serial, e)
        print(f"设备 {device_serial} 运行程序异常: {e}")

def run(device_list, ks_id, current_comment_id):
    global stop_flag, slide_count, config, tasks, switchers

    # 启动已连接的设备
    futures = []
    for device in device_list:
        if device.strip() == "":
            continue
        device_serial = device.strip().split("\t")[0]
        device_state = device.strip().split("\t")[1]
        print(device_serial, device_state)
        if device_state != "device":
            print(f"{device_serial} 设备未连接")
            continue

        # 如果已经存在 AppSwitcher 实例，更新快手号
        if device_serial in switchers:
            switchers[device_serial].update_ks_id(ks_id, current_comment_id)
        else:
            # 创建 AppSwitcher 实例
            switchers[device_serial] = AppSwitcher(device_serial, [0, 1], ks_id, config['app_switch_interval'], current_comment_id, user_slide_count)

        # 提交任务到线程池
        future = executor.submit(run_main, switchers[device_serial], device_serial)
        futures.append(future)
        tasks[device_serial] = future
        print(f"{device_serial}，提交任务到线程池")

    # 等待所有任务完成
    for future in as_completed(futures):
        try:
            future.result()
        except Exception as e:
            print(f"设备任务异常: {e}")
            log.error("设备任务异常: %s", e)
    print("当前所有任务都执行完成")

    stop_flag = False
    slide_count = 0

def stop_app(device_list):
    global config
    threads = []
    # 启动已连接的设备
    for device in device_list:
        if device.strip() == "":
            continue
        device_serial = device.strip().split("\t")[0]
        device_state = device.strip().split("\t")[1]
        print(device_serial, device_state)
        if device_state != "device":
            print(f"{device_serial} 设备未连接")
            continue
        # 获取 AppSwitcher 实例
        switcher = switchers.get(device_serial)
        if switcher:
            # 创建线程
            thread = threading.Thread(target=switcher.stop_app, daemon=True)
            # 启动线程
            thread.start()
            # 等待所有线程完成
            threads.append(thread)

    # 等待所有线程完成
    for t in threads:
        t.join()
    print("所有线程都执行完成")

def monitor_device_changes(interval=5):
    global device_list, device_futures
    while main_switch:
        new_device_list = run_adb_command("adb devices").splitlines()[1:]
        #print(f"当前设备列表: {new_device_list}")
        if set(new_device_list) != set(device_list):
            log.info("设备列表已更新: %s", new_device_list)
            print(f"设备列表已更新: {new_device_list}")
            thread = threading.Thread(target=handle_device_changes, args=(new_device_list,))
            thread.start()
        time.sleep(interval)

def handle_device_changes(new_device_list):
    global device_list, config, current_ks_id, current_comment_id, tasks, switchers
    existing_devices = set(device_list)
    new_devices = set(new_device_list) - existing_devices
    removed_devices = existing_devices - set(new_device_list)
    # 设备列表发生变化，更新全局设备列表
    device_list = new_device_list

    # 处理新增设备
    for device in new_devices:
        if device.strip() == "":
            continue
        device_serial = device.strip().split("\t")[0]
        device_state = device.strip().split("\t")[1]
        print(device_serial, device_state)
        if device_state != "device":
            print(f"{device_serial} 设备未连接")
            continue
        print("检测到新设备:", new_devices)
        # 如果不存在 AppSwitcher 实例则新建
        if device_serial not in switchers:
            # 创建 AppSwitcher 实例
            switchers[device_serial] = AppSwitcher(device_serial, [0, 1], current_ks_id, config['app_switch_interval'], current_comment_id, user_slide_count)
        else:
            print("存在AppSwitcher 实例，需要清理旧任务")
            task_to_cancel = tasks[device_serial]
            task_to_cancel.cancel()  # 取消旧任务
            del tasks[device_serial]

        # 提交任务到线程池
        future = executor.submit(run_main, switchers[device_serial], device_serial)
        tasks[device_serial] = future
        print(f"{device_serial}，提交任务到线程池")

    # 处理移除设备
    for device in removed_devices:
        device_serial = device.strip().split("\t")[0]
        device_state = device.strip().split("\t")[1]
        print("检测到设备移除:", removed_devices)
        # 获取 AppSwitcher 实例
        switcher = switchers.get(device_serial)
        if switcher:
            del switchers[device_serial]
            del tasks[device_serial]
    print("设备列表更新完成")

def next_account():
    global current_ks_id, current_comment_id, config
    for i, account in enumerate(config['ks_id_list']):
        if account['num'] > 0:
            if current_ks_id != '' and account['ks_id'] != current_ks_id:
                current_comment_id = 0  # 重置 comment_id
            current_ks_id = account['ks_id']

            if current_comment_id >= account['num']:
                current_comment_id = 0  # 重置 comment_id
            current_comment_id += 1

            print(f"Processing ks_id: {current_ks_id}, num: {account['num']}, comment_id: {current_comment_id}")

            return

    # 如果所有 num 都为 0，重置 config
    config = read_config_from_yaml("switcher_config.yml")
    current_ks_id = config['ks_id_list'][0]['ks_id']
    config['ks_id_list'][0]['num'] -= 1
    current_comment_id = 1
    print(f"重置 ks_id: {current_ks_id}, num: {account['num'] + 1}, comment_id: {current_comment_id}")

def main():
    global current_ks_id, main_switch, device_list, config, current_comment_id, log
    print(f"current_ks_id: {current_ks_id}, main_switch: {main_switch}, stop_flag: {stop_flag}, device_list: {device_list}, current_comment_id: {current_comment_id}, tasks: {tasks}, switchers: {switchers}")

    if log is None:
        log = Logger(log_name="控制中心日志").get_logger()
    main_switch = True
    log.info("启动程序")
    # ADB 命令，这里列出所有已连接的设备
    command = "adb devices"
    # 执行 ADB 命令
    device_list = run_adb_command(command).splitlines()[1:]
    # 统计设备
    total_devices(device_list)
    if len(device_list) == 0 or (len(device_list) == 1 and device_list[0].strip() == ""):
        log.error("没有设备连接")
        print("没有设备连接")
        return
    # 读取配置文件
    config = read_config_from_yaml("switcher_config.yml")
    # 创建定时任务线程
    timer_thread = threading.Thread(target=check_slide_count, args=(config['times'],), daemon=True)
    timer_thread.start()
    # 启动设备变化监听线程
    device_monitor_thread = threading.Thread(target=monitor_device_changes, daemon=True)
    device_monitor_thread.start()
    # 程序运行定时器
    timer = threading.Thread(target=stop_main, args=(config['run_interval'],), daemon=True)
    timer.start()
    # 连接控制所有已连接设备
    while main_switch:
        if len(device_list) == 1 and device_list[0] == "":
            time.sleep(5)
            continue
        next_account()
        log.info("当前刷的是快手号: %s 评论区第%s条评论的账号, 线程：%s", current_ks_id, current_comment_id, current_thread().name)
        print(f"当前刷的是快手号: {current_ks_id} 评论区第{current_comment_id}条评论的账号")
        run(device_list, current_ks_id, current_comment_id)

def stop_main(run_interval):
    global main_switch, stop_flag, current_comment_id, config
    time.sleep(run_interval * 60)
    main_switch = False
    stop_flag = True
    log.info("停止程序")
    print("停止程序")
    # 创建并启动定时器线程
    stop_app(device_list)

# 按装订区域中的绿色按钮以运行脚本。
if __name__ == '__main__':
    main()
