import time
import random

from script.personification.personification_operation import PersonificationOperation

class KuaishouOperation(PersonificationOperation):
    def __init__(self, device_serial, package_name='com.smile.gifmaker'):
        super().__init__(device_serial, package_name)
        self.device_name = f"设备 {device_serial}"
        print(f"{self.device_name} 快手操作类初始化")

    def like_video(self):
        print(f"{self.device_name} 点赞视频")
        # 点赞视频
        self.device(resourceId=f"{self.package_name}:id/like_icon").click()

    def comment_on_video(self, comment_text):
        # 评论视频
        comment_count = self.device(resourceId=f"{self.package_name}:id/comment_count_view").get_text()
        comment_icon = self.device(resourceId=f"{self.package_name}:id/comment_icon")
        if comment_count == "抢首评":
            if comment_icon.exists(timeout=5):
                comment_icon.click()
                self.device(resourceId=f"{self.package_name}:id/editor").send_keys(comment_count)
                self.device(text="发送").click(timeout=5)
            else:
                print(f"{self.device_name} 评论失败")
                return
        else:
            if comment_icon.exists(timeout=5):
                comment_icon.click()
            else:
                print(f"{self.device_name} 评论失败")
                return
            comment_view = self.device(resourceId=f"{self.package_name}:id/editor_holder_text")
            if comment_view.exists(timeout=5):
                comment_view.click()
                editor_view = self.device(resourceId=f"{self.package_name}:id/editor")
                if editor_view.exists(timeout=5):
                    editor_view.send_keys(comment_text)
                    self.device(text="发送").click(timeout=5)

        time.sleep(3)
        self.device.press("back")

    def save_video(self):
        print(f"{self.device_name} 收藏视频")
        # 收藏视频
        self.device(resourceId=f"{self.package_name}:id/collect_icon").click()

    def share_video(self):
        print(f"{self.device_name} 转发视频")
        # 转发视频
        self.device(resourceId=f"{self.package_name}:id/forward_icon").click()
        share_video = self.device(text="推荐给朋友").sibling(resourceId=f"{self.package_name}:id/image")
        if share_video.exists(timeout=5):
            share_video.click()
        else:
            time.sleep(3)
            self.device.press("back")

    def browse_comments(self):
        print(f"{self.device_name} 浏览评论")
        # 获取评论数量
        text = self.device(resourceId=f"{self.package_name}:id/comment_count_view").get_text()
        try:
            comment_count = int(text.split(' ')[0])
        except (ValueError, IndexError):
            print(f"{self.device_name} 无法获取评论数量")
            return
        if comment_count == 0:
            print(f"{self.device_name} 该视频暂无评论")
            return

        # 打开评论区
        self.device(resourceId=f"{self.package_name}:id/comment_icon").click()
        time.sleep(5)
        # 根据评论数量决定滑动次数
        max_swipes = min(max(1, comment_count // 10), 20)
        swipe_num = random.uniform(1, max_swipes)
        for _ in range(int(swipe_num)):
            self.swipe()
            wait_time = random.uniform(3, 10)  # 随机生成1到3秒之间的等待时间
            time.sleep(wait_time)  # 每次滑动后等待随机时间
            sub_comment_more = self.device(resourceId=f"{self.package_name}.comment_detail:id/sub_comment_more")
            if sub_comment_more.exists(timeout=5):
                if random.random() < 0.5:  # 50% 的概率点击
                    sub_comment_more.click()
        # 返回上一级
        self.device.press("back")
        if self.device(resourceId=f"{self.package_name}:id/profile_feed_title").exists(timeout=3):
            print(f"{self.device_name} 右侧出现遮盖视图，返回")
            self.device.press("back")

    def view_homepage(self):
        if self.device(resourceId=f"{self.package_name}:id/slide_play_cdn_living_tip").exists(timeout=5):
            print(f"{self.device_name} 正在直播，跳过查看主页")
            return
        slide_play_link_exists = self.device(resourceId=f"{self.package_name}:id/slide_play_right_link_icon").exists(
            timeout=5)
        if slide_play_link_exists:
            print(f"{self.device_name} 出现广告，跳过查看主页")
            return
        slide_home_view = self.device(resourceId=f"{self.package_name}:id/follow_avatar_view")
        slide_home_view_exists = slide_home_view.exists(timeout=5)
        if slide_home_view_exists:
            print(f"{self.device_name} 查看用户主页")
            # 查看用户主页
            self.device(resourceId=f"{self.package_name}:id/user_name_text_view").click()
            self.homepage_slide_operations()
        print(f"{self.device_name} 退出用户主页")


    def homepage_direct_swipe(self, duration_minutes):
        # 进入用户主页直接滑动
        start_time = time.time()
        end_time = start_time + duration_minutes * 60
        while time.time() < end_time:
            self.swipe()
            time.sleep(random.uniform(3, 5))
            if self.device(text="没有更多作品了").exists(timeout=3):
                break
        self.device.press("back")

    def homepage_click_swipe(self, duration_minutes):
        start_time = time.time()
        end_time = start_time + duration_minutes * 60

        if not self.device(resourceId=f"{self.package_name}:id/player_cover_container").exists:
            return

        # 点击用户视频滑动浏览
        self.device(resourceId=f"{self.package_name}:id/player_cover_container").click()
        time.sleep(5)
        while time.time() < end_time:
            self.swipe()
            if self.device(resourceId=f"{self.package_name}:id/profile_feed_title").exists(timeout=3):
                print(f"{self.device_name} 右侧出现遮盖视图，返回")
                self.device.press("back")
            if self.device(text="无更多作品").exists(timeout=3):
                break
            time.sleep(random.uniform(3, 5))

        while not self.device(resourceId=f"{self.package_name}:id/tab_text").exists(timeout=3):
            self.device.press("back")
        self.device.press("back")

    def follow_user(self):
        print(f"{self.device_name} 关注用户")
        # 关注用户
        self.device(resourceId=f"{self.package_name}:id/follow_button").click()

    def search_and_browse(self, keywords, duration_minutes=60):
        keyword_duration = PersonificationOperation.get_keyword_duration(keywords, duration_minutes)
        if not keyword_duration:
            print(f"{self.device_name} 关键词为空")
            return

        first_keyword = next(iter(keyword_duration))
        self.perform_search(first_keyword)

        for key, value in keyword_duration.items():
            if key != first_keyword:
                self.perform_search(key)
            print(f"{self.device_name} 当前搜索的关键词:{key}, 操作时长为：{value}分钟")
            half_duration_minutes = value / 2
            self.browse_search_results(half_duration_minutes)
            self.browse_videos(half_duration_minutes)

    def perform_search(self, keyword):
        search_views = [
            (f"{self.package_name}:id/search_btn", "点击首页搜索视图"),
            (f"{self.package_name}:id/nasa_featured_default_search_view", "点击精选搜索视图"),
            (f"{self.package_name}:id/search_result_text", "下一个关键词搜索")
        ]
        for resource_id, message in search_views:
            search_view = self.device(resourceId=resource_id)
            if search_view.exists(timeout=5):
                print(f"{self.device_name} {message}")
                search_view.click()
                break

        editor_view = self.device(resourceId=f"{self.package_name}:id/editor")
        if editor_view.exists(timeout=5):
            editor_view.send_keys(keyword)
            search_button = self.device(text="搜索")
            if search_button.exists(timeout=5):
                search_button.click()

    def browse_search_results(self, duration_minutes):
        print(f"{self.device_name} 浏览综合页面的内容")
        self.device(className="android.view.View", index=0).click()
        start_time = time.time()
        end_time = start_time + duration_minutes * 60
        while time.time() < end_time:
            self.swipe()
            time.sleep(random.uniform(3, 10))
            if self.device(text="没有更多了").exists(timeout=3):
                break

    def browse_content(self, duration_minutes=60):
        self.device(text="精选").click(timeout=5)
        time.sleep(5)
        start_time = time.time()
        end_time = start_time + duration_minutes * 60
        while time.time() < end_time:
            self.swipe()
            self.call_methods_with_probability(self.probabilities)
            time.sleep(2)

    def browse_videos(self, duration_minutes):
        print(f"{self.device_name} 浏览视频或图片或直播")
        start_time = time.time()
        end_time = start_time + duration_minutes * 60
        numbers = list(range(1, 6))
        while time.time() < end_time and numbers:
            chosen_number = random.choice(numbers)
            if chosen_number == 5:
                if 1 in numbers:
                    numbers.remove(1)
            numbers.remove(chosen_number)
            self.device(className="android.view.View", index=chosen_number).click(timeout=5)
            video_view = self.device(resourceId=f"{self.package_name}:id/cover_container")
            if video_view.exists(timeout=5):
                if self.device(text="附近直播").exists(timeout=5):
                    video_view.click(timeout=5)
                    print(f"{self.device_name} 进入直播间")
                    while time.time() < end_time:
                        time.sleep(5)
                    search_result_view = self.device(resourceId=f"{self.package_name}:id/search_result_text")
                    search_result_view_exists = search_result_view.exists(timeout=5)
                    while not search_result_view_exists:
                        print(f"{self.device_name} 搜索框不存在，返回")
                        self.device.press("back")
                        search_result_view_exists = search_result_view.exists(timeout=5)
                else:
                    video_view.click(timeout=5)
                    time.sleep(3)
                    while time.time() < end_time:
                        self.swipe()
                        self.call_methods_with_probability(self.probabilities)
                        time.sleep(2)
                    self.device.press("back")

                print(f"{self.device_name} 结束当前关键词的浏览操作")
                break

if __name__ == '__main__':
    kuaishou_op = KuaishouOperation("P202408093563")
    kuaishou_op.perform_operations()
