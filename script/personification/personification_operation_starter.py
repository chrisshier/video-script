from script.personification.kuaishouLite_operation import KuaishouLiteOperation
from script.personification.kuaishou_operation import KuaishouOperation
from script.utils.device import get_devices, total_devices
import threading
import time
from concurrent.futures import ThreadPoolExecutor, as_completed

# 设备列表
device_list = []
# 任务列表
tasks = {}
# 线程池
executor = ThreadPoolExecutor(max_workers=10)  # 初始最大线程数
# 设置操作应用
operation_type = 'ks' # ks 快手，ks_lite 快手极速版
# 运行标志
is_running = True

def run():
    global device_list, is_running, tasks
    print(f"启动养号脚本")
    device_list = get_devices()
    # 启动已连接的设备
    futures = []
    if not device_list:
        print("没有设备")
        return
    total_devices(device_list)
    # 启动设备变化监听线程
    device_monitor_thread = threading.Thread(target=monitor_device_changes, daemon=True)
    device_monitor_thread.start()
    for device in device_list:
        if device.strip() == "":
            continue
        device_serial = device.strip().split("\t")[0]
        device_state = device.strip().split("\t")[1]
        print(device_serial, device_state)
        if device_state != "device":
            print(f"{device_serial} 设备未连接")
            continue
        # 创建实例
        tasks[device_serial] = get_operation_object(device_serial)
        # 提交任务到线程池
        future = executor.submit(operation, tasks[device_serial])
        futures.append(future)
        print(f"\n{device_serial}，提交任务到线程池")
    # 等待所有任务完成
    for future in as_completed(futures):
        try:
            future.result()
        except Exception as e:
            print(f"设备任务异常: {e}")
    is_running = False
    tasks = {}
    print("当前所有任务都执行完成")

def monitor_device_changes(interval=5):
    global device_list
    while is_running:
        new_device_list = get_devices()
        # print(f"当前设备列表: {new_device_list}")
        if set(new_device_list) != set(device_list):
            print(f"设备列表已更新: {new_device_list}")
            thread = threading.Thread(target=handle_device_changes, args=(new_device_list,))
            thread.start()
        time.sleep(interval)

def handle_device_changes(new_device_list):
    global device_list, tasks
    existing_devices = set(device_list)
    new_devices = set(new_device_list) - existing_devices
    removed_devices = existing_devices - set(new_device_list)
    # 设备列表发生变化，更新全局设备列表
    device_list = new_device_list

    # 处理新增设备
    for device in new_devices:
        if device.strip() == "":
            continue
        device_serial = device.strip().split("\t")[0]
        device_state = device.strip().split("\t")[1]
        print(device_serial, device_state)
        if device_state != "device":
            print(f"{device_serial} 设备未连接")
            continue
        print("检测到新设备:", new_devices)
        # 如果不存在 AppSwitcher 实例则新建
        if device_serial not in tasks:
            # 创建实例
            tasks[device_serial] = get_operation_object(device_serial)
        else:
            print("存在实例，需要清理旧任务")
            task_to_cancel = tasks[device_serial]
            task_to_cancel.cancel()  # 取消旧任务
            del tasks[device_serial]

        # 提交任务到线程池
        future = executor.submit(operation, tasks[device_serial])
        tasks[device_serial] = future
        print(f"{device_serial}，提交任务到线程池")

    # 处理移除设备
    for device in removed_devices:
        device_serial = device.strip().split("\t")[0]
        device_state = device.strip().split("\t")[1]
        print("检测到设备移除:", removed_devices)
        # 获取实例
        task = tasks.get(device_serial)
        if task:
            del tasks[device_serial]
    print("设备列表更新完成")

def operation(object):
    object.perform_operations()

def get_operation_object(device_serial):
    # 创建实例
    if operation_type == 'ks':
        return KuaishouOperation(device_serial)
    elif operation_type == 'ks_lite':
        return KuaishouLiteOperation(device_serial)
    else:
        raise ValueError("无效的操作类型")

if __name__ == '__main__':
    run()
