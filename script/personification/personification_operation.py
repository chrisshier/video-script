import time
import uuid

import uiautomator2 as u2
from abc import ABC, abstractmethod
import random

from script.utils.config import read_config_from_yaml


class PersonificationOperation(ABC):
    def __init__(self, device_serial, package_name):
        print(f"{device_serial} 基类初始化")
        self.device_name = f"设备 {device_serial}"
        self.device = u2.connect(device_serial)
        self.package_name = package_name
        self.probabilities = {}
        self.timing = 30
        self.keywords = ''

    def start_app(self, package_name):
        # 启动或附加到指定的应用程序会话
        print(f"{self.device_name} 启动应用程序: {package_name}")
        self.device.session(package_name, attach=True)

    def stop_app(self, package_name):
        print(f"{self.device_name} 停止应用程序: {package_name}")
        self.device.app_stop(package_name)

    def swipe(self):
        # 获取屏幕尺寸
        x, y = self.device.window_size()
        # 计算屏幕中心的x坐标
        x_center = x / 2
        # 计算滑动起点y坐标
        y_start = y * 0.8
        # 计算滑动终点y坐标
        y_end = y * 0.1
        # 执行滑动操作
        print(f"{self.device_name} 执行滑动操作")
        self.device.swipe(x_center, y_start, x_center, y_end, 0.1)

    @staticmethod
    def get_keyword_duration(keywords, total_duration_minutes):
        # 将字符串按 "|" 分割成列表
        key_word_list = keywords.split("|")
        if len(key_word_list) == 0:
            return {}
        # 初始化字典
        keyword_duration = {}
        # 为每个关键词分配随机分钟数
        remaining_duration = total_duration_minutes
        for i in range(len(key_word_list) - 1):
            keyword = key_word_list[i]
            # 为当前关键词分配随机分钟数，确保剩余分钟数足够分配给最后一个关键词
            minutes = random.randint(1, remaining_duration - (len(key_word_list) - i - 1))
            keyword_duration[keyword] = minutes
            remaining_duration -= minutes

        # 最后一个关键词分配剩余的所有分钟数
        keyword_duration[key_word_list[-1]] = remaining_duration

        return keyword_duration

    @abstractmethod
    def like_video(self):
        """点赞视频"""
        pass

    @abstractmethod
    def comment_on_video(self, comment_text):
        """评论视频"""
        pass

    @abstractmethod
    def save_video(self):
        """收藏视频"""
        pass

    @abstractmethod
    def share_video(self):
        """转发视频"""
        pass

    @abstractmethod
    def browse_comments(self):
        """浏览评论区"""
        pass

    @abstractmethod
    def view_homepage(self):
        """查看用户主页"""
        pass

    @abstractmethod
    def follow_user(self):
        """关注用户"""
        pass

    @abstractmethod
    def search_and_browse(self, keywords, duration_minutes):
        """搜索并浏览内容"""
        pass

    @abstractmethod
    def browse_content(self, duration_minutes):
        """浏览内容"""
        pass

    def call_methods_with_probability(self, probabilities):
        """
        根据概率调用其他方法。

        :param probabilities: 其他方法调用的概率字典，例如 {'like': 0.5, 'comment': 0.3, 'save': 0.4, 'share': 0.2, 'browse_comments': 0.6, 'view_homepage': 0.7, 'follow': 0.8}
        """
        if self.device(text="点击进入直播间").exists(timeout=10):
            print(f"{self.device_name} 跳过直播")
            return
        if self.device(text="朋友推荐").exists(timeout=2):
            self.device.press("back")

        probability_map = {
            'like': {'like_video': f'{self.package_name}:id/like_icon'},
            'comment': {'comment_on_video': f'{self.package_name}:id/comment_icon'},
            'save': {'save_video': f'{self.package_name}:id/collect_icon'},
            'share': {'share_video': f'{self.package_name}:id/forward_icon'},
            'browse_comments': {'browse_comments': f'{self.package_name}:id/comment_count_view'},
            'view_homepage': {'view_homepage': f'{self.package_name}:id/user_name_text_view'},
            'follow': {'follow_user': f'{self.package_name}:id/follow_button'}
        }
        for probability_key, values in probability_map.items():
            for method_name, element_id in values.items():
                try:
                    element = self.device(resourceId=element_id)
                    if not element.exists(timeout=5):
                        print(f"{self.device_name} 未找到id {element_id}，跳过方法 {method_name} ")
                        continue
                    if random.random() < probabilities.get(probability_key, 0):
                        if method_name == 'comment_on_video':
                            comment_text = "来了"
                            print(f"{self.device_name} 评论视频: {comment_text}")
                            getattr(self, method_name)(comment_text)
                        else:
                            getattr(self, method_name)()
                except Exception as e:
                    print(f": 出现异常{e}，跳过方法{method_name} ")

    def perform_operations(self):
        """
        执行一系列操作，每次随机选择调用“浏览内容”或“搜索并浏览内容”，其他方法根据概率调用。
        :param keywords: 搜索查询字符串
        """
        try:
            config = read_config_from_yaml("personification_config.yml")
            self.timing = config['timing']
            self.probabilities = config['probabilities']
            self.keywords = config['keywords']
            self.stop_app(self.package_name)
            self.start_app(self.package_name)
            time.sleep(5)
            # 随机选择调用“浏览内容”或“搜索并浏览内容”
            if random.choice([True, False]):
                print(f"{self.device_name} 浏览内容模式")
                self.browse_content(self.timing)
            else:
                print(f"{self.device_name} 搜索并浏览内容")
                self.search_and_browse(self.keywords, self.timing)
        except Exception as e:
            #self.device.screenshot(f'{self.device_name}{uuid.uuid4()}.png')
            print(f"{self.device_name} 发生异常: {e}")
        finally:
            self.stop_app(self.package_name)

    def homepage_slide_operations(self):
        try:
            if self.device(text="朋友推荐").exists(timeout=10):
                print(f"{self.device_name} 返回上一页")
                self.device.press("back")
            if self.device(resourceId=f"{self.package_name}.im_plugin:id/navi_half_page_left").exists(timeout=2):
                self.device.press("back")
            # 随机选择调用“进入主页直接滑动”或“点击主页视频滑动”
            if random.choice([True, False]):
                print(f"{self.device_name} 进入主页直接滑动")
                self.homepage_direct_swipe(1)
            else:
                print(f"{self.device_name} 点击主页视频滑动")
                self.homepage_click_swipe(1)
        except Exception as e:
            #self.device.screenshot(f'{self.device_name}{uuid.uuid4()}.png')
            print(f"{self.device_name} 发生异常: {e}")

    def homepage_direct_swipe(self, duration_minutes):
        """ 进入用户主页直接滑动 """
        pass

    def homepage_click_swipe(self, duration_minutes):
        """ 点击用户主页视频滑动 """
        pass



