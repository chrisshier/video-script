from script.personification.personification_operation_starter import run
from script.switcher.app_switcher_starter import main
from script.utils.config import read_config_from_yaml

if __name__ == "__main__":
    switcher_config =  read_config_from_yaml("switcher_config.yml")
    personification_config = read_config_from_yaml("personification_config.yml")
    personification_operation_starter = True
    while True:
        if personification_operation_starter and personification_config['timing'] > 0:
            print("开始养号操作")
            run()
            personification_operation_starter = False
            print("结束养号操作")
        elif switcher_config['run_interval'] > 0:
            print("开始刷视频")
            main()
            personification_operation_starter = True
            print("结束刷视频")
        else:
            print("没有设置运行时间")
            break
    print("结束运行")
